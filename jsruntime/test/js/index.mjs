/**
 * @license
 * Copyright 2021 Du Tian Wei
 * SPDX-License-Identifier: Apache-2.0
 */

import * as obvm from '../../runtime/vm.mjs'
import * as obdebugger from '../../runtime/debugger.mjs'
import * as obcanvaslib from './canvas.mjs'
import * as obaudiolib from './audio.mjs'
import * as oblib_network from './network.mjs'
import * as util from '../../runtime/util.mjs'

let logDiv = null;//document.getElementById('logDiv');
let logs = [];
/**
 * @type {HTMLCanvasElement}
 */
let stage = document.getElementById("stage");
let grid, lastGrid;
function updateStageSize() {
    let styled = window.getComputedStyle(stage);
    stage.height = parseInt(styled.height);
    stage.width = parseInt(styled.width);
}
updateStageSize();
let obcanvas = new obcanvaslib.OBCanvas2D(stage);
let obaudio = new obaudiolib.OBAudio();
let nativeLibs = [obcanvas.install.bind(obcanvas), obaudio.install.bind(obaudio), oblib_network.OBNetwork.install];

var scriptArrayBuffer;
// var phaserSceneJSONStr;
let loadedScript, assets;
let vm;
let fsm;
let startTimestamp;
let lastAnimationFrameTimestamp;
/**
 * 检测长时间运行
 */
let frameStart;
function v2(e) {
    let t = e.currentTarget;
    let height = t.height;
    let width = t.width;
    let x = e.clientX;
    let y = e.clientY;
    let swidth = t.getBoundingClientRect().width;
    let sheight = t.getBoundingClientRect().height;
    let sx = x / swidth * width;
    let sy = y / sheight * height;
    sx = Math.floor(sx);
    sy = Math.floor(sy);
    return new obcanvaslib.Vector2(sx, sy);
}

function postErrorMessage(e) {
    if (e.message === 'func_timeout') {
        if (window.parent) {
            window.parent.postMessage({
                cmd: 'msg',
                arg: {
                    type: 'error',
                    content: 'func_timeout'
                }
            }, '*');
        }
    } else {
        throw e;
    }
}
function ob_event(name, argType, arg) {
    if (vm && vm.isRunning()) {
        vm.BroadcastMessage(new obvm.OBEventMessage(name, argType, arg, null));
        updateVM();
    }
}
stage.addEventListener('touchstart', (e) => {
    ob_event('touchstart', 'Vector2', v2(e));
}, false);
stage.addEventListener('mousedown', (e) => {
    ob_event('touchstart', 'Vector2', v2(e));
}, false);
stage.addEventListener('touchmove', (e) => { ob_event('touchmove', 'Vector2', v2(e)) }, false);
stage.addEventListener('touchcancel', (e) => { ob_event('touchcancel', 'Vector2', v2(e)) }, false);
stage.addEventListener('touchend', (e) => { ob_event('touchend', 'Vector2', v2(e)) }, false);
stage.addEventListener('mouseup', (e) => { ob_event('touchend', 'Vector2', v2(e)) }, false);
stage.addEventListener('mousemove', (e) => {
    let v = v2(e);
    ob_event('mousemove', 'Vector2', v);
    if (window.parent) {
        window.parent.postMessage({
            cmd: 'mousemove',
            arg: v
        }, '*');
    }
}, false);
stage.addEventListener('click', (e) => { ob_event('click', 'Vector2', v2(e)) }, false);
stage.addEventListener('longpress', (e) => { ob_event('longpress', 'Vector2', v2(e)) }, false);
stage.addEventListener('swipe', () => { ob_event('swipe') }, false);
document.addEventListener('keydown', (e) => {
    ob_event('keydown', 'String', e.key)
}, false);
document.addEventListener('keyup', (e) => {
    ob_event('keyup', 'String', e.key)
}, false);
function restart(_fsm) {
    if (vm) {
        vm = null;
    }
    obaudiolib.OBAudio.reset();
    obcanvas.canvas2dctx.restore();
    if (loadedScript) {
        obcanvas.canvas2dctx.clearRect(0, 0, obcanvas.canvas.width, obcanvas.canvas.height);
        obcanvas.canvas2dctx.save();
        vm = new obvm.OBVM(loadedScript, {
            setTimeout: setTimeout.bind(window), Output: debugLog,
            assets: assets || {}
        });
        // vm.Output = alert.bind(window);
        let fsmname = _fsm || document.getElementById("input_start_fsm").value;
        fsm = vm.CreateFSM(fsmname);
        if (!fsm) {
            debugLog("No FSM named " + fsmname);
        } else {
            window.parent.postMessage({
                cmd: 'msg',
                arg: null
            }, '*');
        }
        updateVM();
    }
}
const inputElement = document.getElementById("input_script");
if (inputElement) {
    inputElement.addEventListener("change", () => {
        const fileList = inputElement.files;
        if (fileList.length == 0) {
            return;
        }
        let reader = new FileReader();
        reader.onload = (evt) => {
            stage.width = stage.width;
            scriptArrayBuffer = reader.result;
            loadedScript = obvm.OBScriptLoader.loadScript(scriptArrayBuffer, nativeLibs);
        };
        reader.readAsArrayBuffer(fileList[0]);
    }, false);
}
const runButton = document.getElementById('button_run');
if (runButton) {
    runButton.onclick = () => { restart() };
}
let AnimationFrameEvt = new obvm.OBEventMessage("animationframe", null, null, null);
function updateVM(updateFrame) {
    if (vm && vm.isRunning()) {
        frameStart = performance.now();
        try {
            if (vm.update()) {
                if (updateFrame && fsm) {
                    fsm.PostMessage(AnimationFrameEvt);
                }
            }
        } catch (e) {
            postErrorMessage(e);
        }
        if (lastAnimationFrameTimestamp && frameStart - lastAnimationFrameTimestamp > 500) {
            lastAnimationFrameTimestamp = frameStart;
            updateAnimation();
        }
    }
}
setInterval(updateVM, 100);
function updateAnimation() {
    if (grid != lastGrid) {
        lastGrid = grid;
        obcanvas.canvas2dctx.clearRect(0, 0, obcanvas.canvas.width, obcanvas.canvas.height);
    }
    updateVM(true);
    if (lastGrid) {
        /**
         * @type {CanvasRenderingContext2D}
         */
        let c2dctx = obcanvas.canvas2dctx;
        c2dctx.save();
        let gridX = lastGrid.x;
        let gridY = lastGrid.y;
        let width = obcanvas.canvas.width;
        let height = obcanvas.canvas.height;
        c2dctx.setLineDash([15, 15]);
        c2dctx.lineWidth = 2;
        if (gridX > 0) {
            for (let x = gridX; x < width; x += gridX) {
                c2dctx.fillStyle = 'black'
                c2dctx.lineDashOffset = 0;
                c2dctx.beginPath();
                c2dctx.moveTo(x, 0);
                c2dctx.lineTo(x, height);
                c2dctx.stroke();
                c2dctx.closePath();
                c2dctx.beginPath();
                c2dctx.fillStyle = 'white'
                c2dctx.lineDashOffset = 5;
                c2dctx.moveTo(x, 0);
                c2dctx.lineTo(x, height);
                c2dctx.closePath();
            }
        }
        if (gridY > 0) {
            for (let y = gridY; y < height; y += gridY) {
                c2dctx.fillRect(0, y, width, 1);
                c2dctx.fillStyle = 'black'
                c2dctx.lineDashOffset = 0;
                c2dctx.beginPath();
                c2dctx.moveTo(0, y);
                c2dctx.lineTo(width, y);
                c2dctx.stroke();
                c2dctx.closePath();
                c2dctx.beginPath();
                c2dctx.fillStyle = 'white'
                c2dctx.lineDashOffset = 5;
                c2dctx.moveTo(0, y);
                c2dctx.lineTo(width, y);
                c2dctx.closePath();
            }
        }
        c2dctx.restore();
    }
}
function step(timestamp) {
    if (!startTimestamp) {
        startTimestamp = timestamp;
    }
    lastAnimationFrameTimestamp = timestamp;
    updateAnimation();
    window.requestAnimationFrame(step);
}
window.requestAnimationFrame(step);
function debugLog(m, type, level, stackpath) {
    console.log(m);
    if (window.parent) {
        window.parent.postMessage({
            cmd: 'log',
            arg: {
                msg: String(m),
                stackpath, type, level
            }
        }, '*');
    }
    if (logDiv) {
        if (typeof (m) === 'object') {
            m = JSON.stringify(m);
        }
        logs.unshift(String(m));
        if (logs.length > 250) {
            logs.length = 250;
        }
        logDiv.innerText = logs.join('\n');
    }
}
window.runProject = function (projData) {
    obaudiolib.OBAudio.reset();
    stage.width = stage.width;
    scriptArrayBuffer = projData.bytes;
    loadedScript = obvm.OBScriptLoader.loadScript(scriptArrayBuffer, nativeLibs, {
        debugger: new obdebugger.OBDebugger(),
        instructionWrap(instruction) {
            let pausable = obdebugger.OBDebugger.pausableInstructionWrap(instruction);
            return function (st) {
                if (performance.now() - frameStart > 1000) {
                    st.fsm.VM.pause();
                    throw new obvm.VMPausedException('func_timeout');
                }
                return pausable.apply(null, arguments)
            };
        }
    });
    assets = {};
    let _assets = projData.assets;
    let finished = 0;
    let wait = 0;
    for (let name in _assets) {
        let found = false;
        let imgsuffixs = ['.png', '.jpg', '.jpeg'];
        let lname = name.toLowerCase();
        for (let suffixs in imgsuffixs) {
            if (lname.endsWith(imgsuffixs[suffixs])) {
                wait++;
                let img = new Image();
                let base64 = util.arrayBufferToBase64(_assets[name]);
                let fileType = util.fileType(name).substring(1);
                img.src = 'data:image/' + fileType + ';base64,' + base64;
                img.onload = () => {
                    finished++;
                    if (finished == wait) {
                        restart(projData.fsm);
                    }
                };
                img.onerror = () => {
                    finished++;
                    if (finished == wait) {
                        restart(projData.fsm);
                    }
                };
                assets[name] = img;
                break;
            }
        }
        if (!found) {
            let soundsuffixs = ['.wav', '.mp3'];
            for (let suffixs in soundsuffixs) {
                let audioCtx = obaudiolib.OBAudio.mainAudioCtx;
                if (lname.endsWith(soundsuffixs[suffixs])) {
                    wait++;
                    audioCtx.decodeAudioData(_assets[name], (buffer) => {
                        assets[name] = buffer;
                        finished++;
                        if (finished == wait) {
                            restart(projData.fsm);
                        }
                    }, (e) => {
                        console.error("Error with decoding audio data" + e.err + ":" + name);
                        finished++;
                        if (finished == wait) {
                            restart(projData.fsm);
                        }
                    });
                    found = true;
                    break;
                }
            }
        }
    }

    if (finished == wait) {
        restart(projData.fsm);
    }
}
if (window.parent) {
    let messageHandler = {
        runProject(evt) {
            runProject(evt.data);
        },
        restart(evt) {
            restart(evt.data.fsm);
        },
        showLog(evt) {
            logDiv.style.display = evt.data.value ? 'block' : 'none';
        },
        clearLog() {
            logs.length = 0;
            if (logDiv) {
                logDiv.innerText = "";
            }
        },
        pause() {
            vm.pause();
        },
        resume() {
            vm.resume();
        },
        drawGrid(evt) {
            let arg = evt.data.arg;
            grid = arg;
        }
    };
    window.addEventListener("message", receiveMessage);

    function receiveMessage(event) {
        let cmd = event.data.cmd;
        if (messageHandler[cmd]) {
            messageHandler[cmd](event);
        }
    }

    (function () {
        var throttle = function (type, name, obj) {
            obj = obj || window;
            var running = false;
            var func = function () {
                if (running) { return; }
                running = true;
                requestAnimationFrame(function () {
                    obj.dispatchEvent(new CustomEvent(name));
                    running = false;
                });
            };
            obj.addEventListener(type, func);
        };

        /* init - you can init any event */
        throttle("resize", "optimizedResize");
    })();

    // handle event
    window.addEventListener("optimizedResize", function () {
        updateStageSize();
        // obcanvas.canvas2dctx.save();
        // restart();
        ob_event('windowResize', 'null');
    });

}
try {
    // let ext = await import('./jsruntime.ext.mjs');
    // if (ext && ext.default && ext.default.install) {
    //     nativeLibs.push(ext.default.install);
    // }
    if (window.JSRExt) {
        nativeLibs.push(window.JSRExt.install);
    }
} catch (e) {
    console.info(e);
}