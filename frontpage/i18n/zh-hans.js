// This file was automatically generated.  Do not modify.

/*jshint -W117 */
Blockly.Msg["WeiXin"] = "微信小程序";
Blockly.Msg["NativeRun"] = "本地执行";

Blockly.Msg['TEXT_KEY_VALUE'] = "按键值 %1 %2";
Blockly.Msg["Character Keys"] = "字符键";
Blockly.Msg["Special Values"] = "特殊值";
Blockly.Msg["Modifier Keys"] = "修饰键";
Blockly.Msg["Whitespace Keys"] = "空白键";
Blockly.Msg["Navigation Keys"] = "导航键";
Blockly.Msg["Editing Keys"] = "编辑键";
Blockly.Msg["UI Keys"] = "UI键";
Blockly.Msg["Device Keys"] = "设备键";
Blockly.Msg["IME and Composition Keys"] = "输入法与组合键";
Blockly.Msg["Function Keys"] = "功能键";
Blockly.Msg["Phone Keys"] = "移动设备键";
Blockly.Msg["Multimedia Keys"] = "多媒体键";
Blockly.Msg["Audio Control Keys"] = "音频控制键";
Blockly.Msg["TV Control Keys"] = "电视控制键";
Blockly.Msg["Media Controller Keys"] = "媒体控制键";
Blockly.Msg["Speech Recognition Keys"] = "语音识别键";
Blockly.Msg["Document Keys"] = "文档键";
Blockly.Msg["Application Selector Keys"] = "应用选择键";
Blockly.Msg["Browser Control Keys"] = "浏览器控制键";
Blockly.Msg["Numeric Keypad Keys"] = "数字键盘键";


/*jshint sub:true*/

Blockly.Msg["StartPoint"] = "起点";
Blockly.Msg["CenterPoint"] = "中心";
Blockly.Msg["unityengine_Vertical"] = "垂直";
Blockly.Msg["unityengine_Horizontal"] = "水平";
Blockly.Msg["TEXT_CHARAT_TAIL"] = "";
Blockly.Msg["TEXT_GET_SUBSTRING_TAIL"] = "";
Blockly.Msg["LISTS_GET_INDEX_TAIL"] = "";
Blockly.Msg["LISTS_GET_SUBLIST_TAIL"] = "";
Blockly.Msg["ORDINAL_NUMBER_SUFFIX"] = "";
Blockly.Msg["PROCEDURES_DEFNORETURN_DO"] = "";


Blockly.Msg['DuplicatedEvent'] = "重复监听事件";
Blockly.Msg['SearchBlock'] = "查找块";


Blockly.Msg['name'] = "名称";
Blockly.Msg['title'] = "标题";
