OpenBlock.I18N.START_SRC_NAME = 'Start';
OpenBlock.I18N.NEW_SRC_NAME = 'New Module';
OpenBlock.I18N['Click to set project name'] = 'Click to set project name';
OpenBlock.I18N.START_FSM_NAME = 'Main';
OpenBlock.I18N.NEW_FSM_TYPE_NAME = 'New FSM Type';
OpenBlock.I18N.NEW_STATE_NAME = 'State';
OpenBlock.I18N.NEW_STRUCT_NAME = 'Struct';
OpenBlock.I18N.NEW_FUNCTION_NAME = 'Function';
OpenBlock.I18N.Number = 'Number';
OpenBlock.I18N.Integer = 'Integer';
OpenBlock.I18N.Boolean = 'Boolean';
OpenBlock.I18N.String = 'String';
OpenBlock.I18N.FSM = 'FSM';
OpenBlock.I18N.COLOR = 'Color';

Blockly.Msg["value"] = "value";
Blockly.Msg["VALUE"] = "Value";

Blockly.Msg["Colour"] = "Color";
Blockly.Msg["Number"] = "Number";
Blockly.Msg["String"] = "String";
Blockly.Msg["Boolean"] = "Boolean";
Blockly.Msg["Integer"] = "Integer";
Blockly.Msg["MESSAGE"] = "Message";
Blockly.Msg["FSM_CTRL"] = "状态机控制";
Blockly.Msg["EVENT"] = "Event";
Blockly.Msg["SEARCH"] = "Search";
Blockly.Msg["CLEAR"] = "Clear";
Blockly.Msg["DEBUG"] = "Debug";
Blockly.Msg["TEXT"] = "Text";
Blockly.Msg["BOOLEAN"] = "Boolean";
Blockly.Msg["LOOPS"] = "Loops";
Blockly.Msg["MATH"] = "Math";
Blockly.Msg["LISTS"] = "List";
Blockly.Msg["STRUCTS"] = "Structs";
Blockly.Msg["METHOD"] = "Method";
Blockly.Msg["COLLECT"] = "Collect";
Blockly.Msg["ASSETS"] = "Assets";
Blockly.Msg["FSM"] = "FSM";
Blockly.Msg["Start"] = "Start";
Blockly.Msg["Restore"] = "Restore";
Blockly.Msg["Grid"] = "Grid";
Blockly.Msg["LOGIC_VAR_ASSIGNED"] = "The variable is assigned %1";
Blockly.Msg["LOGIC_VAR_ASSIGNED_TOOLTIP"] = "判断变量是否已经赋值（不支持数字和整数类型）";
Blockly.Msg["NEW_STRUCTS"] = "create struct";
Blockly.Msg["DEF_STRUCTS"] = "定义数据结构";
Blockly.Msg["OP_STRUCTS"] = "操作数据结构";
Blockly.Msg["OP_STRUCT_LIST"] = "操作列表";
Blockly.Msg["OP_STRUCT_SMAP"] = "操作字符串映射";
Blockly.Msg["OP_STRUCT_IMAP"] = "操作整数映射";
Blockly.Msg["TEXT_LOG"] = "记录 %2 级日志 %1";
OpenBlock.I18N.PRIMARY_TYPES = [
    [OpenBlock.I18N.Number, 'Number'],
    [OpenBlock.I18N.Integer, 'Integer'],
    [OpenBlock.I18N.Boolean, 'Boolean'],
    [OpenBlock.I18N.String, 'String'],
    [OpenBlock.I18N.FSM, 'FSM'],
    [OpenBlock.I18N.COLOR, 'Colour']
];
Blockly.Msg["COLOR"] = OpenBlock.I18N.COLOR;
Blockly.Msg["colour"] = OpenBlock.I18N.COLOR;
Blockly.Msg["text"] = OpenBlock.I18N.String;
Blockly.Msg["math_integer"] = "Integer %1";
Blockly.Msg["destroy_fsm"] = "Destroy FSM";
Blockly.Msg["variables_self"] = "Current FSM";
Blockly.Msg["logic_is_not_valid"] = "%1 is not valid";
Blockly.Msg["logic_is_valid"] = "%1 is valid";
Blockly.Msg["controls_yield"] = "yield";
Blockly.Msg["change_state"] = "change state %1";
Blockly.Msg["push_state"] = "push state %1";
Blockly.Msg["pop_state"] = "pop state";
Blockly.Msg["on_message"] = "On message received %1";
Blockly.Msg["received_message_arg"] = "received %2 %1";
Blockly.Msg["on_message_with_arg"] = "On message received %1 with %2";
Blockly.Msg["on_message_struct"] = "On message received %1 with struct %2";
Blockly.Msg["on_message_primary"] = "On message received %1 with primary %2";
Blockly.Msg["fsm_variables_get"] = "get FSM variable %2 %1";
Blockly.Msg["fsm_variables_set"] = "set FSM variable %3 %1 为 %2";
Blockly.Msg["state_variables_get"] = "状态变量 %2 %1";
Blockly.Msg["state_variables_set"] = "设置状态变量 %3 %1 为 %2";
Blockly.Msg["fsm_create"] = "创建 %1 类型的状态机";
Blockly.Msg["fsm_create_unnamed"] = "未命名";
Blockly.Msg["find_fsm_by_type"] = "搜索类型为 %1 的状态机";
Blockly.Msg["struct_count_in_dataset"] = "数据集中 %1 的数量";

Blockly.Msg["Network_join"] = "加入网络";
Blockly.Msg["Network_is_joined"] = "已加入网络";
Blockly.Msg["Network_leave"] = "断开网络";
Blockly.Msg["Network_send_message"] = "发送远程消息";
Blockly.Msg["Network_target"] = "远程目标";
Blockly.Msg["Network_on_received"] = "当收到远程消息";
Blockly.Msg["network_peer_join"] = "当远程目标可用";
Blockly.Msg["network_peer_leave"] = "当远程目标离线";
Blockly.Msg["Network_is_network_message"] = "当前消息是否来自于远程";
Blockly.Msg["Network_enable"] = "接收网络消息";
Blockly.Msg["Network_set_enable"] = "设置当前状态机接收网络消息";
Blockly.Msg["Network_is_enabled"] = "当前状态机是否接受网络消息";


Blockly.Msg["target"] = "target";
Blockly.Msg["data"] = "data";
Blockly.Msg["any"] = "any";
Blockly.Msg["enabled"] = "enabled";
Blockly.Msg["offset"] = "offset";

Blockly.Msg['Simulator'] = 'Simulator';
Blockly.Msg['logic_operation_and'] = "AND";

Blockly.Msg['math_text_to_integer'] = "text to integer %1";
Blockly.Msg['math_text_to_number'] = "text to number %1";

//blockly原版

Blockly.Msg["TEXT_GET_SUBSTRING_END_FROM_END"] = "到倒数第#个字符";
Blockly.Msg["MATH_ROUND_OPERATOR_ROUNDDOWN"] = "向下取整";
Blockly.Msg["MATH_ROUND_OPERATOR_ROUNDUP"] = "向上取整";
Blockly.Msg["MATH_RANDOM_INT_TITLE"] = "随机整数[%1,%2]";


//
//黑色
//预留颜色、547eac、629c1e、9c8c1e、ad4f4f
OpenBlock.setAllBlockStyles({
    "native_call": {
        "colourPrimary": "#1e9c45"
    },
    "control_blocks": {
        "colourPrimary": "#a5745b"
    },
    "operation_blocks": {
        "colourPrimary": "#47868a"
    },
    "message_blocks": {
        "colourPrimary": "#5b6ba5"
    },
    "event_blocks": {
        "colourPrimary": "#a55b5b"
    },
    "string_blocks": {
        "colourPrimary": "#44904a"
    },
    "list_blocks": {
        "colourPrimary": "#5263ab"
    },
    "array_blocks": {
        "colourPrimary": "#0fBD8C"
    },
    "gameobject_blocks": {
        "colourPrimary": "#1e9c45"
    },
    "component_blocks": {
        "colourPrimary": "#9e8144"
    },
    "canvas_blocks": {
        "colourPrimary": "#a56c5b"
    },
    "physical_blocks": {
        "colourPrimary": "#8a6b22"
    },
    "time_blocks": {
        "colourPrimary": "#257976"
    },
    "scene_blocks": {
        "colourPrimary": "#944d67"
    },
    "mobile_blocks": {
        "colourPrimary": "#ab6e8b"
    },
    "output_blocks": {
        "colourPrimary": "#3863a3"
    },
    "input_blocks": {
        "colourPrimary": "#3f8c80"
    },
    "screen_blocks": {
        "colourPrimary": "#bc7e4d"
    },
    "application_blocks": {
        "colourPrimary": "#ab6e6e"
    },
    "audio_blocks": {
        "colourPrimary": "#575E75"
    },
    "date_blocks": {
        "colourPrimary": "#825aa6"
    },
    "LoadAssets_blocks": {
        "colourPrimary": "#408a49"
    },
    "textmeshpro_blocks": {
        "colourPrimary": "#a16c38"
    },
    "network_blocks": {
        "colourPrimary": "#a05aa6"
    },
    "thirdparty_blocks": {
        "colourPrimary": "#8b644b"
    },
    "camera_blocks": {
        "colourPrimary": "#1cb479"
    },
    "database_blocks": {
        "colourPrimary": "#625eaa"
    },
    "rank_blocks": {
        "colourPrimary": "#956732"
    },
    "struct_blocks": {
        "colourPrimary": "#3c8170"
    },
    "colour_blocks": {
        "colourPrimary": "20"
    },
    "logic_blocks": {
        "colourPrimary": "#a5745b"
    },
    "loop_blocks": {
        "colourPrimary": "#a5745b"
    },
    "math_blocks": {
        "colourPrimary": "#47868a"
    },
    "navmesh_blocks": {
        "colourPrimary": "#327e74"
    },
    "procedure_blocks": {
        "colourPrimary": "#375a7d"
    },
    "text_blocks": {
        "colourPrimary": "#44904a"
    },
    "variable_blocks": {
        "colourPrimary": "#377d5b"
    },
    "variable_dynamic_blocks": {
        "colourPrimary": "#7f4c5f"
    },
    "variable_diy_blocks": {
        "colourPrimary": "#71377d"
    },
    "hat_blocks": {
        "colourPrimary": "330",
        "hat": "cap"
    },

    "shadow_blocks": {
        "colourPrimary": "#404040"
    },
})
//分组颜色-黑

OpenBlock.setCategoryStyle("control_category", {
    "colour": "#a5745b"
})
OpenBlock.setCategoryStyle("native_call_category", {
    "colour": "#1e9c45"
})
OpenBlock.setCategoryStyle("navmesh_category", {
    "colour": "#327e74"
})
OpenBlock.setCategoryStyle("operation_category", {
    "colour": "#47868a"
})
OpenBlock.setCategoryStyle("message_category", {
    "colour": "#5b6ba5"
})
OpenBlock.setCategoryStyle("string_category", {
    "colour": "#44904a"
})
OpenBlock.setCategoryStyle("list_category", {
    "colour": "#5263ab"
})
OpenBlock.setCategoryStyle("event_category", {//1d7835
    "colour": "#a55b5b"
})
OpenBlock.setCategoryStyle("array_category", {
    "colour": "#0fBD8C"
})
OpenBlock.setCategoryStyle("gameobject_category", {
    "colour": "#388757"
})
OpenBlock.setCategoryStyle("component_category", {
    "colour": "#9e8144"
})
OpenBlock.setCategoryStyle("canvas_category", {
    "colour": "#a56c5b"
})
OpenBlock.setCategoryStyle("physical_category", {
    "colour": "#8a6b22"
})
OpenBlock.setCategoryStyle("time_category", {
    "colour": "#257976"
})
OpenBlock.setCategoryStyle("scene_category", {
    "colour": "#944d67"
})
OpenBlock.setCategoryStyle("mobile_category", {
    "colour": "#ab6e8b"
})
OpenBlock.setCategoryStyle("output_category", {
    "colour": "#3863a3"
})
OpenBlock.setCategoryStyle("input_category", {
    "colour": "#3f8c80"
})
OpenBlock.setCategoryStyle("screen_category", {
    "colour": "#bc7e4d"
})
OpenBlock.setCategoryStyle("application_category", {
    "colour": "#ab6e6e"
})
OpenBlock.setCategoryStyle("audio_category", {
    "colour": "#575E75"
})
OpenBlock.setCategoryStyle("date_category", {
    "colour": "#825aa6"
})
OpenBlock.setCategoryStyle("LoadAssets_category", {
    "colour": "#408a49"
})
OpenBlock.setCategoryStyle("textmeshpro_category", {
    "colour": "#a16c38"
})
OpenBlock.setCategoryStyle("network_category", {
    "colour": "#a05aa6"
})
OpenBlock.setCategoryStyle("thirdparty_category", {
    "colour": "#8b644b"
})
OpenBlock.setCategoryStyle("camera_category", {
    "colour": "#1cb479"
})
OpenBlock.setCategoryStyle("database_category", {
    "colour": "#625eaa"
})
OpenBlock.setCategoryStyle("rank_category", {
    "colour": "#3c8170"
})
OpenBlock.setCategoryStyle("diytype_category", {
    "colour": "#518377"
})
OpenBlock.setCategoryStyle("colour_category", {
    "colour": "20"
})
OpenBlock.setCategoryStyle("logic_category", {
    "colour": "210"
})
OpenBlock.setCategoryStyle("loop_category", {
    "colour": "120"
})
OpenBlock.setCategoryStyle("math_category", {
    "colour": "#47868a"
})
OpenBlock.setCategoryStyle("procedure_category", {
    "colour": "375a7d"
})
OpenBlock.setCategoryStyle("text_category", {
    "colour": "160"
})
OpenBlock.setCategoryStyle("variable_category", {
    "colour": "377d5b"
})
OpenBlock.setCategoryStyle("variable_dynamic_category", {
    "colour": "7f4c5f"
})
OpenBlock.setCategoryStyle("variable_diy_category", {
    "colour": "#71377d"
})
OpenBlock.setCategoryStyle("variable_state_category", {
    "colour": "#7d3737"
})
OpenBlock.setCategoryStyle("parameter_category", {
    "colour": "#3c377d"
})
OpenBlock.setCategoryStyle("struct_category", {
    "colour": "#3c377d"
})
OpenBlock.setCategoryStyle("variable_fsm_category", {
    "colour": "#3c377d"
})
OpenBlock.setCategoryStyle("assets_category", {
    "colour": "#3c377d"
})
OpenBlock.setCategoryStyle("collect_category", {
    "colour": "#3c377d"
})